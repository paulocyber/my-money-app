import React from 'react'
import MenuItem from './menuItem'

export default props => (
    <li className='treeview'>
        <a href>
            <i className={`fa fa-${props.icon}`}></i><span>{props.label}</span>
            <i className='fa fa-anle-left pull-right'></i>
        </a>
        <ul className='treeview-menu'>
            {props.children}
        </ul>
    </li>

)
