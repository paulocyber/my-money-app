import React from 'react'

export default props => (
    <button type={props.type} className={`btn btn-${props.className}`}
        onClick={props.onClickEvent}>
        <i className={`fa fa-${props.icon}`}></i>
    </button>
)