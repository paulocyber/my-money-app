const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
    entry: './src/index.jsx', //arquivo que tem os "modulos" da aplicação para carregamento
    output: {
        path: __dirname + '/public', //pasta de saida do bundle
        filename: './app.js' //bundle gerado
    },
    devServer: {
        port: 8080, //porta do servidor "dev"
        contentBase: './public', //pasta do servidor
    },
    resolve: {
        extensions: ['', '.js', '.jsx'],
        alias: {
            modules: __dirname + '/node_modules',
            jquery: 'modules/admin-lte/plugins/jQuery/jquery-2.2.3.min.js',
            bootstrap: 'modules/admin-lte/bootstrap/js/bootstrap.js'
        }
    },
    plugins: [ 
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new ExtractTextPlugin('app.css')
    ],
    module: {
        loaders: [{ //load do module para babel para transpale
            test: /.js[x]?$/,
            loader: 'babel-loader', //qual o module transpale
            exclude: /node_modules/, //pasta que não ira transpila
            query: {
                presets: ['es2015', 'react'], //o que sera trasnpila
                plugins: ['transform-object-rest-spread']
            }
        }, {
            test: /\.css$/,
            loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
        }, {
            test: /\.woff|.woff2|.ttf|.eot|.svg|.png|.jpg*.*$/,
            loader: 'file'
        }]
    }
}